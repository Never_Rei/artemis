#!/bin/bash

# This script installs the system_stats application stack
# Maintainer: James Washington
# REPO: https://gitlab.com/directiveoperations/artemis

if [[ `id -u` -ne 0 ]]; then 
    echo "Please run as root"
    exit 1
fi

INSTALL_DIR="/etc/system_stats"
CONFIGS_DIR="${INSTALL_DIR}/configs"
SYSTEM_D_DIR="/etc/systemd/system"
RUNTIME=""

function help_message() {
    printf "This script installs the system stats service containers.\n"
}


function check_etc_exists() {
    if [[ -d "$INSTALL_DIR" ]]; then
        printf "Directory ${INSTALL_DIR} exists.\n"
        return 0;
    else
        printf "Could not find ${INSTALL_DIR}. Please ensure it was created.\n"    
        return 1;
    fi
}


function check_etc_configs_exists() {
    if [[ -d "$CONFIGS_DIR" ]]; then
        printf "Directory ${CONFIGS_DIR} exists.\n"
        return 0;
    else
        printf "Could not find ${CONFIGS_DIR}. Please ensure it was created.\n"
        return 1;
    fi
}

function get_container_runtime() {
    if [[ $(which docker) && $(docker --version) ]]; then
        printf "Docker installed. Configuring Docker SystemD Service.\n"
        RUNTIME="docker"
    else
        if [[ $(which podman) && $(podman --version) ]]; then
            printf "Podman installed. Configuring Podman SystemD Service.\n"
            RUNTIME="podman"
        else
            printf "No Contaner Runtime Environment Installed.\n"
            printf "Please Install Either Docker Or Podman.\n"
            exit 1
        fi
    fi
}

function install() {
    get_container_runtime

    printf "Creating Directory Tree in ${INSTALL_DIR}.\n"
    mkdir -p $INSTALL_DIR
    check_etc_exists || exit 1

    printf "Copying Configuration Files.\n"
    cp -R configs/ $INSTALL_DIR
    check_etc_configs_exists || exit 1

    cp docker-compose.yml $INSTALL_DIR
    
    chown -R root:root $INSTALL_DIR

    printf "Configuring Local IP Address.\n"
    LOCAL_IP=$(ip route get 8.8.8.8 | sed -n '/src/{s/.*src *\([^ ]*\).*/\1/p;q}')
    sed -i "s/TARGET/$LOCAL_IP/" ${CONFIGS_DIR}/prometheus/prometheus.yml
    sed -i "s/TARGET/$LOCAL_IP/" ${CONFIGS_DIR}/grafana/datasources/datasources.yaml

    printf "Copying SystemD Services.\n"
    cp systemd/system_stats_${RUNTIME}.service $SYSTEM_D_DIR
    mv ${SYSTEM_D_DIR}/system_stats_${RUNTIME}.service ${SYSTEM_D_DIR}/system_stats.service
    chown root:root ${SYSTEM_D_DIR}/system_stats.service

    printf "Configuring SystemD Services.\n"
    systemctl daemon-reload && systemctl enable system_stats.service
}


function uninstall() {
    get_container_runtime

    printf "Stopping containers.\n"
    cd $INSTALL_DIR
    systemctl disable system_stats.service
    systemctl stop system_stats.service
    if [[ "${RUNTIME}" = "docker" ]]; then
        volumes=$(docker volume ls | grep system_stats | awk '{print $2}')
    else
        volumes=$(podman volume ls | grep system_stats | awk '{print $2}')
    fi
    for volume in $volumes; do 
        printf "Removing Volume: ${volume}\n"
        if [[ "${RUNTIME}" = "docker" ]]; then
            docker volume rm ${volume} && printf "Docker Volume Removal Complete. \n"
        else
            podman volume rm ${volume} && printf "Podman Volume Removal Complete. \n"
        fi        
    done
    rm -rf ${INSTALL_DIR}
    rm -rf ${SYSTEM_D_DIR}/system_stats.service

    printf "Uninstall Complete.\n"
}


case "$1" in 
    install)
        install
        ;;
    
    uninstall)
        uninstall
        ;;

    *)
        echo $"Usage: $0 $1 {install|uninstall}"
        exit 1
esac
