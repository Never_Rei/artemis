
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

{
  "manifestVersion": "2.0.0",
  "signatureType": "community",
  "signedByOrg": "grafana",
  "signedByOrgName": "Grafana Labs",
  "plugin": "novatec-sdg-panel",
  "version": "2.3.1",
  "time": 1622741010689,
  "keyId": "7e4d0c6a708866e7",
  "files": {
    "LICENSE": "97e484c7b8154ff2001363bc8195a1a68fcbe5e2dff01f2a52fc31a352b9d134",
    "module.js.LICENSE.txt": "417e4d9cd1c713dd76240fbcae19324fafce38cf4a98a6fa78ce59c19fe6d1f9",
    "module.js.map": "576bdee7fba19b6b07d629781fae00287d0e68f28a38acea5d114b381efbc540",
    "plugin.json": "de5928308ecbd0aa818f7add8315f392423cb14ea3f228b84ab7c372638f30df",
    "README.md": "b87c595f30536147b4a3734ee2a1de18696e97f36e299487557cc023c56a8036",
    "img/data-example-1.png": "27843d12536a4471ddaa0a12d9526c298fb40c98cb8aeb7fc3a60761afc4d375",
    "img/data-example-2.png": "1efa28c5894e354f266bad65fbe6f1afffae21c49726017ce8b76ea8d642f86a",
    "img/novatec_sdg_panel_logo.svg": "ce39eeff747ebc6b50e0bf9709f5dbe5b7a6fd0ab9b9a3f80c98de2ff5a20e6b",
    "module.js": "a5b7ba3e253c6f84ba2ec2c9be6e717853ac5fd37ebf0a3925ed51536c4cb743",
    "assets/balancer.png": "4661af707d06446bbb072f140eaf8b70cc032a60079bdf022fc6de2bb0ec0455",
    "assets/database.png": "1ad2f833fbf7ef3d6780d3d382dca40e01cd9f1ee10761c3cb000c9f0e9af4f6",
    "assets/http.png": "ce0e44b5628833354852250fce6c3fd49ac0ce5296a21cede4a94aad33f88c49",
    "assets/mainframe.png": "bab107e84794bc6e6850821c7a5f9d2ef77ea6842ee8dddcc9cb0e9e32088f6a",
    "assets/ldap.png": "503357545853251d19f625941cead4df766f7b990e22e0c04a9a11855ce7058c",
    "assets/web.png": "eff5461e123ab9f535873cc49b9b6189ecec25ac4fd2067a6ba0b9d1746dca98",
    "assets/ftp.png": "8326e294512c41dc3ab49584dc8a50fbc0011689c31ba945ab785d824f4e9289",
    "assets/smtp.png": "f714071a2cf1c971b15d4c40879c15425ee5e8cd8a196d4758e78db9159b10dc",
    "assets/default.png": "b6d0e6e83b84f227166f86209e5404c6b70d27444d3d68f6a92e03cc594216fa",
    "assets/message.png": "d26f33d477ae18787f59cd5151e635d43f1cc2b4fa070fe184b984b3e2978f57",
    "partials/module.html": "2588bf2ba71e3ceb3607fd0430c80e808e58fc187077d0957be9ad9b7deebaa8",
    "partials/options.html": "a1ffcb1360f77f0d4d5a8c2aac3c5f9fb1e4a9917485c16790fe37bc28dd441d"
  }
}
-----BEGIN PGP SIGNATURE-----
Version: OpenPGP.js v4.10.1
Comment: https://openpgpjs.org

wqIEARMKAAYFAmC5EBIACgkQfk0ManCIZucNagIJAWslsCZ3IZsDEGmBviUM
BDR6OH+mXeIDJPX0KMz5kq4v12paXGBK1lVj3n8+oCQ4HnbArMgNHvkwOCC+
RsLePBSXAgkBwphQPpqnam3im2+HUd4ZidG/b7Y4Co0yZbSB38D/F4qvq8XE
GI0Vt/FoZpkpRgQihCtt8SsrXoO8nPN0C2BeT4U=
=i1FO
-----END PGP SIGNATURE-----
